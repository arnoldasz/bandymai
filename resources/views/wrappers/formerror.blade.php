@if($errors->has($field))
	<div class="alert alert-danger">
		@foreach($errors->get($field) as $error)
			<p><strong>Klaida!</strong> {{ $error }}</p>
		@endforeach
	</div>
@endif