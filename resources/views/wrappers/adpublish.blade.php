<div class="ad-content" id="{{ $ad->id }}">
	<h4><a href="{{ route('ad.show', ['id' => $ad->id, 'title' => $ad->adRouteTitle()]) }}">{{ $ad->title }}</a></h4>
	<p>{{ $ad->shortDescription() }}</p>
</div>