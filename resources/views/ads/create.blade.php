@extends('layout.layout')

@section('content')

<div class="row marketing" >
	<div class="col-xl-12">
        
        <h3>Pridėti reklamą</h3>
        <hr>

		<form method="POST" action="/ads">

			{{ csrf_field() }}

			<div class="form-group">
				<label for="adTitle">Pavadinimas</label>

				@include('wrappers.formerror', ['field' => 'title'])

				<input type="text" class="form-control" id="adTitle" name="title" required value="{{ old('title') }}">
			</div>

			<div class="form-group">
				<label for="adLink">Adresas</label>

				@include('wrappers.formerror', ['field' => 'url'])

				<input type="text" class="form-control" id="adLink" name="url" required value="{{ old('url') }}">
			</div>

			<div class="form-group">
				<label for="adDescription">Aprašymas</label>
				
				@include('wrappers.formerror', ['field' => 'description'])

				<textarea class="form-control" rows="3" id="adDescription" name="description" required>{{ old('description') }}</textarea>
			</div>

			<button type="submit" class="btn btn-default">Reklamuoti</button>
		</form>

	</div>

</div>

@endsection