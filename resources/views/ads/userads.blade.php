@extends('layout.layout')

@section('content')

<div class="row marketing" >
    <div class="col-xl-12">
        @if(count($ads) > 0)
            @foreach($ads as $ad)
                <h4><a href="/ads/{{$ad->id}}">{{ $ad->title }}</a></h4>
                <p>{{ $ad->shortDescription() }}</p>
            @endforeach
        @else
        	<h4>TU NETURI REKLAMU LOL</h4>
        @endif
    </div>
</div>

@endsection