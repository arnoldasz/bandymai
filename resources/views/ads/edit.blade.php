@extends('layout.layout')

@section('content')

<div class="row marketing" >
	<div class="col-xl-12">
        
        @if(!$ad)
        	{{ redirect('/') }}
        @endif

        <h3>Redaguoti reklama</h3>
        <hr>

		<form method="POST" action="/ads/{{ $ad->id }}">

			{{ csrf_field() }}
			{{ method_field('PATCH') }}

			<div class="form-group">
				<label for="adTitle">Pavadinimas</label>

				@include('wrappers.formerror', ['field' => 'title'])

				<input type="text" class="form-control" id="adTitle" name="title" value="{{ $ad->title }}">
			</div>

			<div class="form-group">
				<label for="adLink">Adresas</label>

				@include('wrappers.formerror', ['field' => 'url'])

				<input type="text" class="form-control" id="adLink" name="url" value="{{ $ad->url }}">
			</div>

			<div class="form-group">
				<label for="adDescription">Aprašymas</label>

				@include('wrappers.formerror', ['field' => 'description'])
				
				<textarea class="form-control" rows="3" id="adDescription" name="description">{{ $ad->description }}</textarea>
			</div>

			<button type="submit" class="btn btn-default">Atnaujinti</button>
		</form>

	</div>

</div>

@endsection