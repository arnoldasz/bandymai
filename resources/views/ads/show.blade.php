@extends('layout.layout')

@section('content')

<div class="row marketing" >
	<div class="col-xl-12">
        <h3>{{ $ad->title }}</h3>
        <hr>

		<a href="{{ $ad->url }}">Apsilankyti puslapyje</a>
		<p></p>
		<p>{{ $ad->description }}</p>
		<hr>
		@if($user->id === $ad->user_id)
			<div>
				<div class="d-inline-block">
					<form action="/ads/{{ $ad->id }}/edit">
						<button type="submit" class="btn btn-default">Atnaujinti</button>
					</form>
				</div>
				<div class="d-inline-block">
					<form method="POST" action="/ads/{{ $ad->id }}">
						{{ csrf_field() }}
						{{ method_field('DELETE') }}
						<button type="submit" class="btn btn-default">Istrinti</button>
					</form>
				</div>
			</div>
		@endif
	</div>

</div>

@endsection