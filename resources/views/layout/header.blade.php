<div class="header clearfix">
    <nav>
        <ul class="nav nav-pills float-right">
            <li class="nav-item">
                <a class="nav-link" href="/">Namai</a>
            </li>
            @if(Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="/userads">Mano reklamos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout">Logout</a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link" href="/login">Prisijungti</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/register">Registruotis</a>
                </li>
            @endif
        </ul>
    </nav>
    <h3 class="text-muted">Reklamuok</h3>
</div>