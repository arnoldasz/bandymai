<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>Naujas reklamų puslapis</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="/css/app.css">
    <!-- Custom styles for this template -->
    <link href="/css/narrow-jumbotron.css" rel="stylesheet">
    <!-- Laravel .js script -->
    <script src="/js/app.js"></script>
    <script src="/js/setup.js"></script>
    <!-- Timer script -->
    <script src="/js/adUpdate.js"></script>
</head>

<body>

    <div class="container">

        @include('layout.header')

        @yield('content')

        <footer class="footer">
            <p>&copy; Company 2017</p>
        </footer>

    </div> <!-- /container -->
</body>
</html>