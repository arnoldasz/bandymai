@extends('layout.layout')

@section('content')
<div class="jumbotron">
    <h1 class="display-3">Reklamuok jau dabar!</h1>
    <p class="lead">Jei nori patalpinti savo puslapio reklamą, spausk reklamuoti.</p>
    <p><a class="btn btn-lg btn-success" href="/ads/create" role="button">Reklamuoti</a></p>
</div>

<div class="row marketing" >
    <div id="ads-container" class="col-xl-12">
        
    </div>
</div>
@endsection