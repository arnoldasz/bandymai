<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Create/edit and show user ads
 */
Route::get('/userads', 'Ads\UserAdsController@showUserAds');
Route::get('/ads/{id}-{title}', 'Ads\UserAdsController@show')->name('ad.show');
Route::resource('ads', 'Ads\UserAdsController', ['except' => 'show']);

/**
 * Home page
 */
Route::get('/', 'Ads\AdsController@index');
/**
 * Ads update response
 */
Route::post('/adsupdate', 'Ads\AdsController@adsListUpdate');

/**
 * User accounts and sessions
 */
Auth::routes();
Route::get("/logout", 'Auth\LoginController@logout');

Route::get('omg/yolo-{id}', function($id){})->name('xddd');