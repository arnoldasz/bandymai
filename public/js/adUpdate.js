var maxId = 0;

window.onload = function () {
	updateAds();
    setInterval(updateAds, 2000);
};

function updateAds()
{ 
	$.ajax({
		type:"POST",
		url:"/adsupdate",
		data: { maxid:maxId },
		datatype:"json",
		success:function(data) {
			/** 
			 * if data returned from server is 0 - there are no new ads.
			 */
			if(data != 0)
			{
				/**
				 * Append ads-container div with ads
				 */
				$.each(data, function(index, element) {
					$("#ads-container").prepend(element.template);

					/**
					 * Search for maxid
					 */
					maxId = element.id > maxId ? element.id : maxId;
				});
			}
		}
	});
}