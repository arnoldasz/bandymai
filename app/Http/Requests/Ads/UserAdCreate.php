<?php

namespace App\Http\Requests\Ads;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class UserAdCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check() ? true : false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:10|max:100',
            'url' => 'required|url|max:100',
            'description' => 'required|min:50'
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Neįvestas reklamos pavadinimas',
            'title.min' => 'Reklamos pavadinimas negali būti trumpesnis nei :min simbolių',
            'title.max' => 'Reklamos pavadinimas negali būti ilgesnis nei :max simbolių',
            'url.required' => 'Neįvesta nuoroda',
            'url.url' => 'Neteisingas nuorodos formatas',
            'url.max' => 'Nuoroda negali būti ilgesnė nei :max simbolių',
            'description.required' => 'Neįvestas aprašymas',
            'description.min' => 'Aprašymas negali būti trumpesnis nei :min simbolių'
        ];
    }
}
