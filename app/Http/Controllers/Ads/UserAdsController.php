<?php

namespace App\Http\Controllers\Ads;

use Auth;
use App\Models\Ad;
use App\Http\Requests\Ads\UserAdCreate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserAdsController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| User Ads Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles users interactions with ads. Users can view,
	| create, edit ant delete ads.
	|
	*/

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	/*
		 * Only registered users will be allowed to access
		 * UserAdsController methods (except 'show')
		 */
    	$this->middleware('auth');
    }

	/**
	 * Show page with ad submit form.
	 * @return ads.create view
     */
    public function create()
    {
    	return view('ads.create');
    }

    /**
	 * Show page with ad edit form.
	 * @param Ad $ad
	 * @return ads.edit view
     */
    public function edit(Ad $ad)
    {
    	return view('ads.edit', compact('ad'));
    }

    /**
	 * Destroy ad which id is given.
	 * @param Ad $ad
	 * @return redirect user to home page
     */
    public function destroy(Ad $ad)
    {
    	/**
		 * Check if ad with given id exists
    	 */
    	if($ad)
    	{
    		$ad->delete();
    	}
    	return redirect('/');
    }

    /**
	 * Show info of Ad with given id.
	 * @param Ad $ad
	 * @return ads/show view
     */
    public function show($id, $title)
    {
    	/**
    	 * Check if $ad exists and return user to home page
    	 * if it not. User and ad objects should be passed to 
    	 * .show page.
    	 */
        $ad = Ad::find($id);

    	if(!$ad)
    	{
    		return redirect('/');
    	}
    	$user = Auth::user();

    	return view('ads.show', compact('ad', 'user'));
    }

    /**
     * Show all user created ads
     * @return userads view
     */
    public function showUserAds()
    {
        $ads = Auth::user()->ads;
        return view('ads.userads', compact('ads'));
    }

    /**
     * Create a new ad and store its information to database
     * @param Request $request
     * @return Redirect user to homepage
     */
    public function store(UserAdCreate $request)
    {
    	/**
    	 * Create new ad from submited form info
    	 */
    	Ad::create([
    		'title' => $request->title,
    		'url' => $request->url,
    		'description' => $request->description,
    		'user_id' => Auth::user()->id
    		]);

    	return redirect('/');
    }

    /**
     * Update ad info
     * @param Request $request
     * @param Ad $ad
     * @return Redirect user to homepage
     */
    public function update(UserAdCreate $request, Ad $ad)
    {
    	/**
    	 * Fill ad with info from submited form
    	 */
    	$ad->fill([
    		'title' => $request->title,
    		'url' => $request->url,
    		'description' => $request->description
    		]);
    	$ad->save();

    	return redirect('/');
    }
}
