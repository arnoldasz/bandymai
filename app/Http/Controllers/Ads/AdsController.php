<?php

namespace App\Http\Controllers\Ads;

use App\Models\Ad;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdsController extends Controller
{
    /*
	|--------------------------------------------------------------------------
	| Ads Controller
	|--------------------------------------------------------------------------
	|
	| This controllers handles request to main ads window.
	|
	*/

	/**
	 * Show page with all created ads.
	 * @return index view
     */
	public function index()
	{
		/**
		 * Get all ads from database and list them by date
		 */
		//$ads = Ad::all()->sortByDesc('created_at');
		
		return view('/index');
	}

	/**
	 * Generate a list of ads that we return to home page
	 * to update ads in real time
	 * @return json array
	 */
	public function adsListUpdate(Request $request)
	{
		$adsList = [];
		$ads = Ad::ofAds($request->maxid)->get();

		foreach($ads as $ad)
		{
			$adsList[] = array_merge(['template' => $ad->template()], ['id' => $ad->id]);
		}
		return count($ads) > 0 ? response()->json($adsList) : 0;
	}
}
