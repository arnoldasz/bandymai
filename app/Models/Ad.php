<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'title', 'url', 'description', 'user_id' ];

    /**
     * Find a owner of an ad
     *
     * @return owner or ad
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Function that returns first 30 characters from ad description
     *
     * @return string
     */
    public function shortDescription($length = 30)
    {
        return strlen($this->description) < $length ? $this->description : substr($this->description, 0, 30).'...';
    }

    /**
     * Function to generate ad template
     *
     * @return string
     */
    public function template()
    {
        $ad = $this;
        return view('wrappers.adpublish', compact('ad'))->render();
    }

    /**
     * Scope that returns new ads
     *
     * @return
     */
    public function scopeOfAds($query, $id)
    {
        return $query->where('id', '>', $id);
    }

    /**
     * Function that returns ad title with spaces replaced by '-'
     *
     * @return string
     */
    public function adRouteTitle()
    {
        return preg_replace('/\s+/', '-', $this->title);
    }
}
